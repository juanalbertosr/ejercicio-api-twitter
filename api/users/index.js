const router = require("express").Router();
const controller = require("./users.controller.js");

router.get("/", controller.getAll);
router.get("/:username", controller.getByUsername);
router.post("/", controller.create);
router.delete("/:username", controller.deleteByUsername);
router.put("/:username", controller.putByUsername);

module.exports = router;