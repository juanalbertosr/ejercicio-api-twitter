const mongoose = require('mongoose');

const config = require("../../.env");
const options = config[process.env.NODE_ENV];
const _MONGOURL = options.MONGOURL;

mongoose.connect(_MONGOURL);
console.log("Connecting MongoDB to", _MONGOURL);

var USERSschema = mongoose.Schema({
    username: {
        String,
        required: [true, "username is required"],
        type: "string",
        maxlength: [20, "username is too long"]
    },
    name: {
        String,
        required: [true, "name is required"],
        type: "string",
        maxlength: [30, "name is too long"]
    },
    email: {
        String,
        required: [true, "email is required"],
        type: "string",
        maxlength: [30, "email is too long"]
    }
});

var USERS = mongoose.model('users', USERSschema);

module.exports = USERS;