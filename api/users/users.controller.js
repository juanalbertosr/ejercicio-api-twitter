const fs = require("fs");

const USERSModel = require('./users.model.js');

/////////////////

function getAll(request, result) {
    USERSModel.find()
        .then(response => {
            return result.json(response);
        })
};

function getByUsername(request, result) {
    USERSModel.findOne({ username: request.params.username })
        .then(response => {
            return result.json(response);
        })
};

function create(request, result) {
    const newUser = request.body;

    const newU = new USERSModel({
        username: newUser.username,
        name: newUser.name,
        email: newUser.email
    });

    const error = newU.validateSync();
    if (error) {
        return result.status(400).json(error.errors);
    } else {
        USERSModel.findOne({ username: newU.username })
            .then(response => {
                if (response) {
                    return result.status(400).json("Usuario duplicado");
                } else {
                    newU.save();
                    return result.status(201).json(newUser);
                }
            });
    }
};

function deleteByUsername(request, result) {
    USERSModel.deleteOne({ username: request.params.username })
        .then(response => {
            return result.json(response);
        })
};

function putByUsername(request, result) {
    const newUser = request.body;

    const newU = new USERSModel({
        username: newUser.username,
        name: newUser.name,
        email: newUser.email
    });

    const error = newU.validateSync();
    if (error) {
        return result.status(400).json(error.errors);
    } else {
        USERSModel.updateOne(
            { "username": request.params.username },
            { $set: { "username": newU.username, "name": newU.name, "email": newU.email } }
        )
            .then(response => {
                return result.status(200).json(response);
            })
    }
};


module.exports.getAll = getAll;
module.exports.getByUsername = getByUsername;
module.exports.create = create;
module.exports.deleteByUsername = deleteByUsername;
module.exports.putByUsername = putByUsername;