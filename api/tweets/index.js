const router = require("express").Router();
const controller = require("./tweets.controller.js");

router.get("/", controller.getAll);
router.get("/sorted", controller.getSorted);
router.get("/:id", controller.getById);
router.post("/", controller.create);
router.delete("/:id", controller.deleteById);
router.patch("/:id", controller.patchById);

module.exports = router;