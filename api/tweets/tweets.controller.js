const fs = require("fs");

const TWEETSModel = require('./tweets.model.js');
const USERSModel = require('../users/users.model.js');

/////////////////

function getAll(request, result) {
    TWEETSModel.find()
        .then(response => {
            return result.json(response);
        })
};

function getById(request, result) {
    TWEETSModel.findOne({ id: request.params.id })
        .then(response => {
            return result.json(response);
        })
};

function create(request, result) {
    const newTweet = request.body;
    const currDate = Date.now();

    const newT = new TWEETSModel({
        username: newTweet.username,
        text: newTweet.text,
        date: currDate
    });

    const error = newT.validateSync();
    if (error) {
        return result.status(400).json(error.errors);
    } else {
        USERSModel.findOne({ username: newT.username })
            .then(response => {
                if (response) {
                    newT.save();
                    return result.status(201).json(newT);
                } else {
                    return result.status(400).json("El usuario no existe");
                }
            });
    }
};

function deleteById(request, result) {
    TWEETSModel.deleteOne({ _id: request.params.id })
        .then(response => {
            return result.json(response);
        })
};

function getSorted(request, result) {
    TWEETSModel.find().sort({ "date": 1 })
        .then(response => {
            return result.json(response);
        })
};

function patchById(request, result) {
    const newTweet = request.body;
    const currDate = Date.now();

    const newT = new TWEETSModel({
        text: newTweet.text,
        date: currDate
    });

    TWEETSModel.updateOne(
        { "_id": request.params.id },
        { $set: { "text": newT.text, "date": newT.date } }
    )
        .then(response => {
            return result.status(200).json(response);
        })
};


module.exports.getAll = getAll;
module.exports.getSorted = getSorted;
module.exports.getById = getById;
module.exports.create = create;
module.exports.deleteById = deleteById;
module.exports.patchById = patchById;