const mongoose = require('mongoose');

const config = require("../../.env");
const options = config[process.env.NODE_ENV];
const _MONGOURL = options.MONGOURL;

mongoose.connect(_MONGOURL);
console.log("Connecting MongoDB to", _MONGOURL);

var TWEETSschema = mongoose.Schema({
    username: {
        type: String,
        required: [true, "username is required"],
        maxlength: [20, "username is too long"]
    },
    text: {
        type: String,
        required: [true, "text is required"],
        maxlength: [280, "tweet is too long"]
    },
    date: {
        type: Number,
        required: [true, "date is required"],
    },
});

var TWEETS = mongoose.model('tweets', TWEETSschema);

module.exports = TWEETS;