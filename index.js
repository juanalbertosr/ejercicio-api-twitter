const express = require("express");
const app = express();
const usersRoute = require("./api/users/index.js");
const tweetsRoute = require("./api/tweets/index.js");
const morgan = require("morgan");
const compression = require('compression');

const config = require("./.env");
const options = config[process.env.NODE_ENV];
const _PORT = options.PORT;

//Middlewares
app.use(express.json());
app.use("/api/users", usersRoute);
app.use("/api/tweets", tweetsRoute);
app.use(morgan("combined"));
app.use(compression());

/////////////////

app.listen(_PORT, (err) => {
    if (err) {
        console.log("Error");
    } else {
        console.log("Servidor arrancado. Escuchando en el puerto:", _PORT);
    }
});